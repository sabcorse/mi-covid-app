library(shiny)
library(stringr)
library(miniUI)
library(tidyverse)
library(ggplot2)
library(DT)
library(RMySQL)
library(shinyBS)
library(jsonlite)
library(leaflet)
library(googlesheets4)
library(revgeo)
library(usmap)
library(sp)
library(maps)
library(maptools)
library(jvamisc)
library(shinyjs)
library(shinyalert)
library(shinyWidgets)
library(shinythemes)
library(fresh)

headingGray <- '#222222'
toggleGray <- '#666666'
white <- '#FFFfff'
backgroundGray <- '#CCCCCC'
alertGreen <- '#5CDB95'
alertRed <- 'FC4445'
buttonBlue <- '#3FEEE6'

my_theme <- create_theme(
  theme = "cosmo",
  bs_vars_color(
		brand_primary = "#FBEC5D" 
		),
  bs_vars_wells(
	       bg = "#FFFFFF"
	       )
)

# Create function to get response times
epochTime <- function() {
	as.integer(Sys.time())
}
humanTime <- function() {
	format(Sys.time(), "%Y%m%d-%H%M%OS")
}

# Create function to label entries as mandatory
labelMandatory <- function(label) {
	tagList(label,
		span("*", class = "mandatory_star")
		)
}
appCSS <- ".mandatory_star { color : red; }"

# Choose mandatory fields
fieldsMandatory1 <- c("email_add", "event_street", "event_city")
fieldsMandatory2 <- c("case_street", "case_city", "case_month", "case_day", "case_year", "case_start", "case_end", "case_type")
fieldsAll1 <- c("email_add", "event_street_num", "event_street", "event_city", "event_zip")
fieldsAll2 <- c("case_street_num", "case_street", "case_city", "case_zip", "case_month", "case_day", "case_year", "case_start", "case_end", "case_type")

# Establish county name choices
county_names <- list('State of Michigan','Alcona','Alger','Allegan','Alpena','Antrim','Arenac',
		     'Baraga',
		     'Barry','Bay','Benzie','Berrien','Branch','Calhoun','Cass','Charlevoix',
		     'Cheboygan','Chippewa','Clare','Clinton','Crawford','Delta','Dickinson',
		     'Eaton','Emmet','Genesee','Gladwin','Gogebic','Grand Traverse','Gratiot',
		     'Hillsdale','Houghton','Huron','Ingham','Ionia','Iosco','Iron','Isabella',
		     'Jackson','Kalamazoo','Kalkaska','Kent','Keweenaw','Lake','Lapeer','Leelanau',
		     'Lenawee','Livingston','Luce','Mackinac','Macomb','Manistee','Marquette',
		     'Mason','Mecosta','Menominee','Midland','Missaukee','Monroe','Montcalm',
		     'Montmorency','Muskegon','Newaygo','Oakland','Oceana','Ogemaw','Ontonagon',
		     'Osceola','Oscoda','Otsego','Ottawa','Presque Isle','Roscommon','Saginaw',
		     'St. Clair','St. Joseph','Sanilac','Schoolcraft','Shiawassee','Tuscola',
		     'Van Buren','Washtenaw','Wayne','Wexford')

# Establish sql credentials
source("credentials.R")
options(mysql=list("host" = host,
		  "port" = 3306,
		  "user" = user,
		  "password" = password
		  ))
databaseName1 <- "loc_reports"
databaseName2 <- "case_reports"
table1 <- "loc_rep_responses"
table2 <- "case_rep_responses"

# Create functions to read and write sql databases
saveData <- function(data, db_num){
	if (db_num == 1) {
		databaseName = databaseName1
		table = table1
	}
	else if (db_num == 2) {
		databaseName = databaseName2
		table = table2
	}
	db <- dbConnect(MySQL(), dbname = databaseName, host = options()$mysql$host, 
			port = options()$mysql$port, user = options()$mysql$user,
			password = options()$mysql$password)
	query <- sprintf("INSERT INTO %s (%s) VALUES ('%s')",
			 table,
			 paste(names(data), collapse = ", "),
			 paste(data, collapse = "', '")
			)
	dbGetQuery(db,query)
	dbDisconnect(db)
}

loadData <- function(db_num){
	if (db_num == 1) {
		databaseName = databaseName1
		table = table1
	}
	else if (db_num == 2) {
		databaseName = databaseName2
		table = table2
	}
	db <- dbConnect(MySQL(), dbname = databaseName, host = options()$mysql$host,
                        port = options()$mysql$port, user = options()$mysql$user,
                        password = options()$mysql$password)

}

# Create function to read in sheet (called when necessary to ensure *up to date* every time)
readGoogle <- function(){
	gs4_auth(path = 'client_secret.json')
	data <- read_sheet('https://docs.google.com/spreadsheets/d/1MSb2fG-OjHhhrpzthJ-dyLTNMtrd7qOltCf56dAGKaY/edit#gid=680812803')
	return(data)
}

ui <- miniPage(
	       tags$head(
    tags$style(HTML("
	#covid_tabs ul li.selected {
	background: #03002e;
	color: #03002e;
	}
	#covid_tabs li {
	display: #1A1A1D;
	background: #1A1A1D;
	}
	#covid_tabs a {
	color:white;
	}	
      #covid_tabs a:active {
        background: #03002e;
	color:#03002e;
      }
      #covid_tabs a:hover {
	background: #1A1A1D;
	color:white;
	}
    "))
  ),
	       use_theme(my_theme), 
	       geoloc::onload_geoloc(),
  useShinyalert(),
  shinyjs::useShinyjs(),
  shinyjs::inlineCSS(appCSS),
  miniTabstripPanel( id = 'covid_tabs',
	    miniTabPanel(div('Live Feed'), icon = icon("info-sign", lib = "glyphicon"),
			 setBackgroundColor(color = "#4E4E50"),
		 miniContentPanel(
		 shinyjs::hidden(div(align = 'center', id = "depLocOutput", style = 'background-color:#03002e;box-shadow:2px 2px 4px white;',
				  tags$br(),
				  h3(tags$b("Michigan COVID-19 Risk Checker", style = "color:white")),
				  #h5(tags$b("Click here to get your county's data:"), style = 'font-family:"Playfair Display"'),
				  actionButton("myBtn", div(h5(tags$b("Get your location's data", style = 'color:black'))), class = "btn btn-primary"), 
				  tags$br(),
				  div(h5('*Please note that location data may not be precise, especially on computers.', style = 'margin-left:11px;margin-right:11px;color:white')),
				  tags$br()
				  )),
		 shinyjs::hidden(div(align = 'center', id = 'indepLocOutput', style = 'margin-bottom:10px;background-color:#03002e;color:white;box-shadow:2px 2px 4px white;',
				  tags$br(),
				  h3(tags$b("Michigan COVID-19 Risk Checker", style = "color:white")), 
				  selectInput("county", div(h5(tags$b("Select a region to get case data:"))),
					      choices=county_names, selected='State of Michigan'),
				  actionButton("getData", div(h5(tags$b("Get Data")), style = 'color:black'), class = "btn btn-primary"),
				  tags$br(), tags$br(), tags$br()
				  )),

				  shinyjs::hidden(div(style = 'background-color:#010048;color:white;margin-top:10px;margin-bottom:10px;box-shadow:2px 2px 4px white;', align = 'center', id = 'chooseLocOutput', 
				     tags$br(), 
				     div(h5(tags$b('Find data for another MI county:'))), 
				     #tags$br(),
				     div(switchInput("indep_loc", label = "Search For a Location", labelWidth = '200px', onLabel = 'Yes', offLabel = 'No')), 
				     tags$br()
				     )),
  		conditionalPanel(align = 'center', condition="$('html').hasClass('shiny-busy')",
                                                   tags$div(style = 'color:white', "Loading...",id="loadmessage")),
	div(align = 'center', style = 'color:white;margin-right:11px;margin-left:11px;',
		    tags$br(),
				  img(src='u-m_logo-hex-withoutline.png', height = '80px', width = 'auto'),
				(h5('Produced by U-M Physics researchers')),
		    h5('Case data retrieved from', tags$a("Johns Hopkins University\'s repository", href = "https://tinyurl.com/vz9pusy")),
		    h5('Threat levels retrieved from the', tags$a("MI Start Map", href="https://www.mistartmap.info/", style = 'color:#FBEC5d')),
		    tags$br()

		    
		    )
			)

    ),
    miniTabPanel(div("About"), icon = icon("bars"),
		 miniContentPanel(div(style = 'background-color:#03002e;color:white;margin-bottom:10px;box-shadow:2px 2px 4px white;', align = 'center', tags$br(),
		     h3(tags$b("About The Project:")), 
		     tags$p(style = 'margin-right:20px;margin-left:20px', "This web application is the result of a U-M Physics group's efforts to make COVID-19 data more accessible to the public. The intent of the site is to make COVID-19 data quick to access and easy to understand so everyone can protect themselves and others."),
			 tags$br()
		     ),
		 div(style = 'background-color:#010048;color:white;margin-bottom:10px;box-shadow:2px 2px 4px white;', align = 'center', tags$br(),
		     h3(tags$b("Contact:")),
		     div(style = 'margin-left:11px;margin-right:11px;', h5(tags$b("Sabrina Corsetti: "), "B.S. Physics, Mathematics ('21); University of Michigan; sabcorse@umich.edu"),
		     h5(tags$b("Ella McCauley: "), "Physics Research Assistant; University of Michigan; mcella@umich.edu"), 
		     h5(tags$b("Thomas Schwarz: "), "Associate Professor of Physics; University of Michigan; schwarzt@umich.edu")), 
                     tags$br()
		     ),
		div(align = 'center', style = 'color:white',
		    tags$br(),
				  img(src='u-m_logo-hex-withoutline.png', height = '80px', width = 'auto'),
				(h5('Produced by U-M Physics researchers')),
		    h5('Case data retrieved from', tags$a("Johns Hopkins University\'s repository", href = "https://tinyurl.com/vz9pusy")),
		    h5('Threat levels retrieved from the', tags$a("MI Start Map", href="https://www.mistartmap.info/", style = 'color:#FBEC5d')),
		    tags$br()
		    
		    )
 
		 ))
    )
    
)

server <- function(input, output, session) {
	# Pseudo-global location variables
	lon <- reactiveVal('')
	lat <- reactiveVal('')
	nice_county <- reactiveVal('')
	this_fips <- reactiveVal('')
	this_confirmed <- reactiveVal('')
	this_new_confirmed <- reactiveVal('')
	this_threat_level <- reactiveVal('')
	this_threat_color <- reactiveVal('')
	loc <- reactiveVal('')
	street_num <- reactiveVal('')
	street <- reactiveVal('')
	city <- reactiveVal('')
	zip <- reactiveVal('')

	output$UM_logo <- renderImage({
	outfile <- tempfile('')})

	observe({
		req(input$geoloc_lon)
		req(input$geoloc_lat)
		lon(as.numeric(input$geoloc_lon))
		lat(as.numeric(input$geoloc_lat))

		if (!is.na(lon()) & !is.na(lat())) {
			loc(revgeo(lon(),lat(),provider='photon',output='frame'))
			country <- loc()$country
			if (country != "United States of America" | country == 'Country Not Found') {
				shinyalert(title = 'Oops!',
                                text = paste0('It looks like you are outside Michigan. If this occurred in error, it is either due to GPS inaccuracies or browser incompatibility. You will be prompted to type or select your location for location-based functions on the site.'),
                                type = "error",
                                confirmButtonText = "Okay",
				confirmButtonCol = "#03002e"
                                )

                                shinyjs::show('indepLocOutput')
			}

			else {
			county_full <- latlong2(data.frame(x=c(lon()), y=c(lat())), to="county")
				if (county_full[1] == 'michigan') {
					this_county <- county_full[2]
					this_fips(fips("MI", county = this_county))
		
					nice_county(str_to_title(this_county))
		
					sheet <- readGoogle()
					this_confirmed(sheet[sheet$fips == this_fips(),]$confirmed)
                			this_new_confirmed(sheet[sheet$fips == this_fips(),]$avg_new_confirmed)
					this_threat_level(sheet[sheet$fips == this_fips(),]$threat_level)
		
		if (this_threat_level() == 'very high risk') {
			this_threat_color('#850a0b')
		}
		else if (this_threat_level() == 'high risk') {
			this_threat_color('#cb2917')
		}
		else if (this_threat_level() == 'medium-high risk') {
			this_threat_color('#FF8503')
		}
		else if (this_threat_level() == 'medium risk') {
			this_threat_color('#Feb204')
		}	
		else if (this_threat_level() == 'low risk') {
			this_threat_color('#176798')
		}
		else if (this_threat_level() == 'post-pandemic') {
			this_threat_color('#1f3f53')
		}
		else{
			this_threat_color('#43464b')
		}

					loc(revgeo(lon(),lat(), provider = "photon", output='frame'))
                			street_num(loc()$housenumber)
                			street(loc()$street)
                			city(loc()$city)
                			zip(loc()$zip)

					shinyjs::show('chooseLocOutput')
					shinyjs::show('depLocOutput')
					shinyjs::show('formLocButton')
				}

				else {
					shinyalert(title = 'Oops!',
                           		text = paste0('It looks like you are outside Michigan. If this occurred in error, it is either due to GPS inaccuracies or browser incompatibility. You will be prompted to type or select your location for location-based functions on the site.'),
                           		type = "error",
                           		confirmButtonText = "Okay",
					confirmButtonCol = "#03002e"
					)
				
				shinyjs::show('indepLocOutput')
				}
			}
		}
		else {
			shinyalert(title = 'Hi!',
                           text = paste0('Your location could not be determined. This is either due to denied permissions or browser incompatibility. You will be prompted to type or select your location for location-based functions on the site.'),
                           type = "error",
                           confirmButtonText = "Okay",
			   confirmButtonCol = "#03002e"
                           )

			shinyjs::show('indepLocOutput')
		}
		
	})

	observeEvent(input$indep_loc, {
		if (input$indep_loc == TRUE) {
			shinyjs::hide('depLocOutput')
			shinyjs::show('indepLocOutput')
		}

		else if (input$indep_loc == FALSE) {
			shinyjs::hide('indepLocOutput')
			shinyjs::show('depLocOutput')
		}
	}, ignoreInit = TRUE)

	observeEvent(input$myBtn, {
		shinyalert(title = nice_county(),
			   text = HTML(paste0('<b>', span(str_to_title(this_threat_level()), style = paste0('font-size:24px; color:', this_threat_color())), '</b>', '<br>',
                                          '<br>', "Confirmed Cases: ", '<b>', this_confirmed(), '</b>', '<br>',
					  "New Cases (1-Week Avg.): ", '<b>', this_new_confirmed(), '</b>')
),
                           type = "info",
			   html = TRUE,
                           confirmButtonText = "Close",
			   confirmButtonCol = "#03002e"
                           )
	})

	observeEvent(input$myBtn2, {
		if (street_num() != 'House Number Not Found'){
			updateTextInput(session, "event_street_num", value = paste(street_num()))
		}
		if (street() != 'Street Not Found'){
                	updateTextInput(session, "event_street", value = paste(street()))
		}
		if (city() != 'City Not Found'){
                	updateTextInput(session, "event_city", value = paste(city()))
		}
		if (zip() != 'Postcode Not Found'){
                	updateTextInput(session, "event_zip", value = paste(zip()))
		}
		if (street_num() == 'House Number Not Found' | street() == 'Street Not Found' | city() == 'City Not Found' | zip() == 'Postcode Not Found'){
			shinyalert(title = 'Hi!',
                           text = 'Some of your location details could not be found. Please fill in any required blanks.',
                           type = "info",
                           confirmButtonText = "Okay",
			   confirmButtonCol = "#03002e"
                           )
		}
	})

	observeEvent(input$getData, {
		this_county <- input$county
		if (this_county == 'State of Michigan') {
			this_fips_selected <- 'Michigan'
		}

		else {
			this_fips_selected <- fips("MI", county = this_county)
		}

		sheet <- readGoogle()

		if (this_county == 'State of Michigan') {
			this_threat_level_selected <- 'Search for individual counties to see threat assessments'
			this_font_size <- '18px'
		}

		else {
			this_threat_level_selected <- str_to_title(sheet[sheet$fips == this_fips_selected,]$threat_level)
			this_font_size <- '24px'
		}

		if (this_threat_level_selected == 'Very High Risk') {
			threat_color_selected = '#850a0b'
		}
		else if (this_threat_level_selected == 'High Risk') {
			threat_color_selected = '#cb2917'
		}
		else if (this_threat_level_selected == 'Medium-High Risk') {
			threat_color_selected = '#FF8503'
		}
		else if (this_threat_level_selected == 'Medium Risk') {
			threat_color_selected = '#Feb204'
		}	
		else if (this_threat_level_selected == 'Low Risk') {
			threat_color_selected = '#176798'
		}
		else if (this_threat_level_selected == 'Post-Pandemic') {
			threat_color_selected = '#1f3f53'
		}
		else{
			threat_color_selected = '#43464b'
		}

		this_confirmed_selected <- sheet[sheet$fips == this_fips_selected,]$confirmed
		this_new_confirmed_selected <- sheet[sheet$fips  == this_fips_selected,]$avg_new_confirmed
		shinyalert(title = this_county,
			   text = HTML(paste0('<b>', span(this_threat_level_selected, style = paste0('font-size:', this_font_size, '; color:', threat_color_selected)), '</b>', '<br><br>',
					  "Confirmed Cases: ", '<b>', this_confirmed_selected, '</b><br>',
                                          "New Cases (1-Week Avg.): ", '<b>', this_new_confirmed_selected, '</b>')
),
                           type = "info",
			   html = TRUE,
                           confirmButtonText = "Close",
			   confirmButtonCol = "#03002e"
                           )
	})

	formData1 <- reactive({
		data <- sapply(fieldsAll1, function(x) {input[[x]]})
		data <- c(data, timestamp = humanTime())
		data <- t(data)
		data
	})

	formData2 <- reactive({
		data <- sapply(fieldsAll2, function(x) {input[[x]]})
		data <- c(data, timestamp = humanTime())
		data <- t(data)
		data
	})

	observeEvent(input$submit1, {
			     shinyjs::disable("submit1")
			     shinyjs::show("submit_msg1")
			     shinyjs::hide("error1")

			     tryCatch({
			     	saveData(formData1(), 1)
			     	shinyjs::reset("form1")
			     	shinyjs::hide("form1")
			     	shinyjs::show("thankyou_msg1")
				},
		     	     
		             error = function(err) {
				     shinyjs::html("error_msg1", err$message)
				     shinyjs::show(id = "error1", anim = TRUE, animType = "fade")
			     },

			     finally = {
				     shinyjs::enable("submit1")
				     shinyjs::hide("submit_msg1")
			     })
	})

	observeEvent(input$submit2, {
			     shinyjs::disable("submit2")
			     shinyjs::show("submit_msg2")
			     shinyjs::hide("error2")

			     tryCatch({
			     	saveData(formData2(), 2)
			     	shinyjs::reset("form2")
			     	shinyjs::hide("form2")
			     	shinyjs::show("thankyou_msg2")
			     },

			     error = function(err) {
				     shinyjs::html("error_msg2", err$message)
				     shinyjs::show(id = 'error2', anim = TRUE, animType = 'fade')
			     },

			     finally = {
				     shinyjs::enable("submit2")
				     shinyjs::hide("submit_msg2")
			     })
	})

	observeEvent(input$submit_another1, {
			     shinyjs::show("form1")
			     shinyjs::hide("thankyou_msg1")
	})
	
	observeEvent(input$submit_another2, {
			     shinyjs::show("form2")
			     shinyjs::hide("thankyou_msg2")
	})

	observe({
		mandatoryFilled <- vapply(fieldsMandatory1,
					  function(x) {
						  !is.null(input[[x]]) && input[[x]] != ""
						  },
					  logical(1))
		mandatoryFilled <- all(mandatoryFilled)
		shinyjs::toggleState(id = "submit1", condition = mandatoryFilled)
	})

	observe({
		mandatoryFilled <- vapply(fieldsMandatory2,
                                          function(x) {
                                                  !is.null(input[[x]]) && input[[x]] != ""
                                          },
                                          logical(1))
                mandatoryFilled <- all(mandatoryFilled)
                shinyjs::toggleState(id = "submit2", condition = mandatoryFilled)
	})
}

shinyApp(ui, server)

